# Readme - Kruskal

Ce projet a pour but de coder l'algorithme de Kruskal pour l'arbre couvrant minimal puis pour la 2-approximation du problème du voyageur de commerce. Vous pouvez voir ci-dessous un exemple d'ensemble de points auquel on a appliqué l'algorithme de Kruskal.

##### Ensemble original de points (planclean.png)
<img src="https://git.eleves.ens.fr/rigaux/AlgoL3-Kruskal/raw/master/planclean.png" width="500px">

##### Arbre convrant minimal sur cet ensemble de points (planacm.png)
<img src="https://git.eleves.ens.fr/rigaux/AlgoL3-Kruskal/raw/master/planacm.png" width="500px">

##### Approximation correspondante du problème du voyageur de commerce (plantsm.png)
<img src="https://git.eleves.ens.fr/rigaux/AlgoL3-Kruskal/raw/master/plantsm.png" width="500px">

## Compilation

`make all` permet de compiler le projet. Les binaires se trouvent dans `build/default/`. `make clean` permet de nettoyer le projet.

## Utilisation

`Kruskal.bin` prend un graphe sur l'entrée standard selon le format:
Les deux premiers entiers décrivent le nombre de noeuds puis le nombre d'arêtes.
Ensuite, pour chaque arête, 3 nombres sont attendu: deux entiers désigants les deux extrèmités de l'arête (numérotés entre `0` et `nbNoeuds-1`), puis un flottant désignant le poids de l'arête. Il va ensuite appliquer kruskal à ce graphe.

`TestPlan.bin` prend un entier `n` sur l'entrée standart. Il va génerer un graphe aléatoire complet dans le plan (selon la distance euclidienne) et va appliquer kruskal à ce graphe.

Les deux executables précédents retournent sur la sortie standard la description de l'arbre couvrant minimal au format DOT.

Les deux executables en `*Bound.bin` prennent la même entrée que `*.bin` et retournent un majorant du nombre d'opérations effectuées par `*.bin` sur la même entrée.

Le script `dot_to_png.sh <name>` sert à convertir le fichier `<name>.dot` vers une image en format png appelée `<name>.dot`.

## Complexité

### Kruskal

Les allocations de données dans Kruskal se font en temps linéaire par rapport au nombre de noeuds et d'arêtes. Le tri se fait en temps `O(m log(m))` où 'm' est le nombre d'arêtes. La construction de l'arbre couvrant minimal via un Union-Find se fait ensuite en `O(n a(n))` où `a` est la fonction inverse d'Ackerman et `n`le nombre de noeud. Mais le nombre de noeuds est au pire linéaire en le nombre d'arêtes, car le graphe est supposé connexe, d'où le fait que l'algorithme de Kruskal ait une complexité temporelle en `O(m ln (m))`.

### 2-approximation du problème du voyageur de commerce

La 2-approximation du problème du voyageur de commerce n'est qu'un parcours en profondeur effectué sur le résultat de Kruskal. L'allocation et l'initialisation des données est en temps linéaire par rapport à l'entrée. On ne rajoute que des opérations en temps constant à chaque visite de noeud. L'approximation a donc la même complexité que Kruskal.

