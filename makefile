
.PHONY: help all

all: build.Kruskal.bin build.KruskalBound.bin build.TestPlan.bin build.TestPlanBound.bin
	@

png: all
	@printf "50\n" > input
	@make run.TestPlan.bin < input >> /dev/null
	@rm -f input
	@./dot_to_png.sh planclean
	@./dot_to_png.sh planacm
	@./dot_to_png.sh plantsm
	@rm -rf *.dot

help:
ifeq (0,${MAKELEVEL})
	@echo "TODO: This message"
else
	@
endif

#################
# CONFIGURATION #
#################

SHELL := /bin/bash

ifeq (0,${MAKELEVEL})
CPP := gcc
CPP_FLAGS := -std=c89 -Wall ${CPP_FLAGS}
LD_FLAGS := -lm ${LD_FLAGS}
endif
export CPP CPP_FLAGS LD_FLAGS

CFG ?= default
ifneq (default,${CFG})
include $(patsubst %,config/%.mk,${CFG})
endif
export CFG

CMD := ${CMD}

BUILD_DIR ?= build/${CFG}
BUILD_DIR := ${BUILD_DIR}
export BUILD_DIR

SRC_DIR ?= src
SRC_DIR := ${SRC_DIR}

########
# FILES

SOURCES += $(shell find ${SRC_DIR}/ -name "*.c")
SOURCES := ${SOURCES}
include $(shell find ${SRC_DIR}/ -name "*.src.mk")
SOURCES := ${SOURCES}

objects = $(filter-out %.c, \
$(patsubst ${SRC_DIR}/%.c,${BUILD_DIR}/%.o,$(1)) \
$(patsubst ${BUILD_DIR}/%.c,${BUILD_DIR}/%.o,$(1)) \
)

import = $(call objects,\
$(foreach pkg,$(1),$(filter ${SRC_DIR}/$(pkg).% ${BUILD_DIR}/$(pkg).%,${SOURCES})))

OBJECTS := $(call objects,${SOURCES})

include $(shell find ${SRC_DIR}/ -name "*.bin.mk")

#########
# RULES #
#########

.PHONY: none nested run run.* build.*
.SECONDARY:

none:
	@

nested:
	@${MAKE} ${CMD}

run:
	@${CMD}

run.% : build.%
	@$(patsubst run.%,./${BUILD_DIR}/%,$@) ${CMD}

build.% : ${BUILD_DIR}/%
	@

########
# Clean

.PHONY: clean

clean:
	@rm -rf build
	@rm -f *.dot
	@rm -f *.png

#########
# BUILD #
#########

include $(shell find . -name "*.d" | grep "^${BUILD_DIR}/")

$(BUILD_DIR)/%.o : ${SRC_DIR}/%.c
	@printf "$@\n"
	@mkdir -p $$(dirname $@)
	@${CPP} ${CPP_FLAGS} $^ -MM -MT $@ -MF $(patsubst %.o,%.d,$@)
	@${CPP} ${CPP_FLAGS} -c $< -o $@

$(BUILD_DIR)/%.bin :
	@printf "$@\n"
	@mkdir -p $$(dirname $@)
	@${CPP} $(filter %.o,$^) -o $@ ${LD_FLAGS}

