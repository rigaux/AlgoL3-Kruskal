#include "Edge.h"

#include <assert.h>
#include <stdlib.h>

Edge_t* Edge_new(int a, int b, double weight, Edge_t* next)
{
	Edge_t* e = malloc(sizeof(Edge_t));
	e->a = a;
	e->b = b;
	e->weight = weight;
	e->next = next;
	return e;
}

int Edge_new_bound(int a, int b, double weight, Edge_t* next)
{
	return 6;
}

int Edge_other(Edge_t* e, int u)
{
	if(e->a == u)
		return e->b;
	assert(e->b == u);
	return e->a;
}

int Edge_other_bound(Edge_t* e, int u)
{
	return 3;
}

void Edge_delete(Edge_t* e)
{
	while(e != NULL)
	{
		Edge_t* next = e->next;
		free(e);
		e = next;
	}
}

int edge_list_size(Edge_t* e)
{
	int size = 1;
	while(e != NULL)
	{
		e = e->next;
		size += 2;
	}
	return size;
}

int Edge_delete_bound(Edge_t* e)
{
	return edge_list_size(e);
}

void Edge_to_dot(Edge_t* e, FILE* file)
{
	if(e == NULL)
		return;
	if(e->a < e->b) /* TODO: remove trailing 0 */
	{
		if(e->weight >= 0)
			fprintf(file, "\t%d -- %d [label=\"%lf\"];\n", e->a, e->b, e->weight);
		/*else
		fprintf(file, "\t%d -- %d [color=\"0.5 0.5 0.5\"];\n", e->a, e->b); */
	}
	Edge_to_dot(e->next, file);
}

int Edge_to_dot_bound(Edge_t* e, FILE* file)
{
	return 4 * edge_list_size(e);
}
