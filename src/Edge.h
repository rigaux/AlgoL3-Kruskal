#pragma once

#include <stdio.h>

struct Edge_t
{
	int a, b;
	double weight;
	struct Edge_t* next;
};
typedef struct Edge_t Edge_t;

Edge_t* Edge_new          (int,     int, double, Edge_t*);
int     Edge_new_bound    (int,     int, double, Edge_t*);

int     Edge_other        (Edge_t*, int);
int     Edge_other_bound  (Edge_t*, int);

void    Edge_delete       (Edge_t*);
int     Edge_delete_bound (Edge_t*);

void    Edge_to_dot       (Edge_t*, FILE*);
int     Edge_to_dot_bound (Edge_t*, FILE*);
