#include "Graph.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "UnionFind.h"

Graph_t* Graph_new(int nbNodes)
{
	double* x = malloc(nbNodes * sizeof(double));
	double* y = malloc(nbNodes * sizeof(double));
	Edge_t** adj = malloc(nbNodes * sizeof(Edge_t*));
	int iNode;
	for(iNode = 0; iNode < nbNodes; ++iNode)
	{
		x[iNode] = -1;
		y[iNode] = -1;
		adj[iNode] = NULL;
	}
	Graph_t* g = malloc(sizeof(Graph_t));
	g->x = x; g->y = y;
	g->adj = adj;
	g->nbNodes = nbNodes;
	g->nbEdges = 0;
	return g;
}

int Graph_new_bound(int nbNodes)
{
	return 14 + nbNodes * 5;
}

void Graph_delete(Graph_t* g)
{
	assert(g != NULL);
	int iNode;
	for(iNode = 0; iNode < g->nbNodes; ++iNode)
		Edge_delete(g->adj[iNode]);
	free(g->x);
	free(g->y);
	free(g->adj);
	free(g);
}

int Graph_delete_bound(Graph_t* g)
{
	int bound = 6;
	int iNode;
	for(iNode = 0; iNode < g->nbNodes; ++iNode)
		bound += 3 + Edge_delete_bound(g->adj[iNode]);
	return bound;
}

void Graph_add_edge(Graph_t* g, int a, int b, double weight)
{
	assert(0 <= a && a < g->nbNodes);
	assert(0 <= b && b < g->nbNodes);
	g->adj[a] = Edge_new(a, b, weight, g->adj[a]);
	g->adj[b] = Edge_new(b, a, weight, g->adj[b]);
	if(weight >= 0)
		++g->nbEdges;
}

int Graph_add_edge_bound(Graph_t* g, int a, int b, double weight)
{
	return 8 + Edge_new_bound(a, b, weight, g->adj[a])
		     + Edge_new_bound(b, a, weight, g->adj[b]);
}

Graph_t* Graph_from_input(FILE* file)
{
	int nbNodes, nbEdges;
	fscanf(file, "%d%d", &nbNodes, &nbEdges);
	Graph_t* g = Graph_new(nbNodes);
	int iEdge;
	for(iEdge = 0; iEdge < nbEdges; ++iEdge)
	{
		int a, b;
		double weight;
		fscanf(file, "%d%d%lf", &a, &b, &weight);
		Graph_add_edge(g, a, b, weight);
	}
	return g;
}

int Graph_from_input_bound(FILE* file)
{
	int bound = 4;
	int nbNodes, nbEdges;
	fscanf(file, "%d%d", &nbNodes, &nbEdges);
	bound += Graph_new_bound(nbNodes);
	Graph_t* g = Graph_new(nbNodes);
	int iEdge;
	for(iEdge = 0; iEdge < nbEdges; ++iEdge)
	{
		int a, b;
		double weight;
		fscanf(file, "%d%d%lf", &a, &b, &weight);
		bound += 6 + Graph_add_edge_bound(g, a, b, weight);
	}
	return bound;
}

void Graph_to_dot(Graph_t* g, FILE* file)
{
	fprintf(file, "graph {\n");
	int iNode;
	for(iNode = 0; iNode < g->nbNodes; ++iNode)
	{
		if(g->x[iNode] >= 0 && g->y[iNode] >= 0)
			fprintf(file, "\t%d[pos=\"%lf,%lf!\"];\n", iNode, g->x[iNode], g->y[iNode]);
		Edge_to_dot(g->adj[iNode], file);
	}
	fprintf(file, "}\n");
}

int Graph_to_dot_bound(Graph_t* g, FILE* file)
{
	int bound = 3;
	int iNode;
	for(iNode = 0; iNode < g->nbNodes; ++iNode)
		bound += 6 + Edge_to_dot_bound(g->adj[iNode], file);
	return bound;
}

const int MIN_QUICK = 100;

void insertsort(Edge_t* tab, int size)
{
	int iEdge;
	for(iEdge = 0; iEdge < size-1; ++iEdge)
	{
		int mini = iEdge;
		int jEdge;
		for(jEdge = iEdge + 1; jEdge < size; ++jEdge)
			if(tab[mini].weight > tab[jEdge].weight)
				mini = jEdge;
		Edge_t tmp = tab[mini];
		tab[mini] = tab[iEdge];
		tab[iEdge] = tmp;
	}
}

int insertsort_bound(Edge_t* tab, int size)
{
	return 1 + size * 7 + size * (size - 1) * 2;
}

int median(Edge_t* tab)
{
	if(tab[0].weight <= tab[1].weight)
	{
		if(tab[1].weight <= tab[2].weight)
			return 1;
		if(tab[0].weight <= tab[2].weight)
			return 2;
		return 0;
	}
	if(tab[0].weight <= tab[2].weight)
		return 0;
	if(tab[1].weight <= tab[2].weight)
		return 2;
	return 1;
}

int median_bound(Edge_t* tab)
{
	return 3;
}

void quicksort(Edge_t* tab, int size)
{
	if(size < MIN_QUICK)
		insertsort(tab, size);
	else
	{
		int pivot = median(tab);
		Edge_t* copy = malloc(size * sizeof(Edge_t));
		int beg = 0, end = size;
		int iEdge;
		for(iEdge = 0; iEdge < size; ++iEdge)
			if(iEdge != pivot)
			{
				if(tab[iEdge].weight <= tab[pivot].weight)
					copy[beg++] = tab[iEdge];
				else
					copy[--end] = tab[iEdge];
			}
		copy[beg] = tab[pivot];
		for(iEdge = 0; iEdge < size; ++iEdge)
			tab[iEdge] = copy[iEdge];
		free(copy);
		quicksort(tab, beg);
		quicksort(tab+beg+1, size - end);
	}
}

int quicksort_bound(Edge_t* tab, int size)
{
	if(size < MIN_QUICK)
		return 1 + insertsort_bound(tab, size);
	int bound = 1;
	int pivot = median(tab);
	bound += median_bound(tab);
	Edge_t* copy = malloc(size * sizeof(Edge_t));
	int beg = 0, end = size;
	int iEdge;
	for(iEdge = 0; iEdge < size; ++iEdge)
		if(iEdge != pivot)
		{
			if(tab[iEdge].weight <= tab[pivot].weight)
				copy[beg++] = tab[iEdge];
			else
				copy[--end] = tab[iEdge];
		}
	bound += 4 + 6 * size;
	copy[beg] = tab[pivot];
	bound += 2 + size * 3;
	bound += quicksort_bound(copy, beg);
	bound += quicksort_bound(copy+beg+1, size - end);
	free(copy);
	bound += 4;
	return bound;
}

Graph_t* Graph_kruskal(Graph_t* g)
{
	assert(g != NULL);
	UnionFind_t* uf = UnionFind_new(g->nbNodes);
	Graph_t* acm = Graph_new(g->nbNodes);
	Edge_t* edges = malloc(g->nbEdges * sizeof(Edge_t));
	int iEdge = 0;
	int iNode;
	for(iNode = 0; iNode < g->nbNodes; ++iNode)
	{
		Edge_t* e = g->adj[iNode];
		while(e != NULL)
		{
			if(e->a < e->b && e->weight >= 0)
				edges[iEdge++] = *e;
			e = e->next;
		}
	}
	quicksort(edges, iEdge);
	for(iEdge = 0; iEdge < g->nbEdges; ++iEdge)
	{
		if(UnionFind_union(uf, edges[iEdge].a, edges[iEdge].b) == 1)
			Graph_add_edge(acm, edges[iEdge].a, edges[iEdge].b, edges[iEdge].weight);
		else
			Graph_add_edge(acm, edges[iEdge].a, edges[iEdge].b, -1);
	}
	UnionFind_delete(uf);
	free(edges);
	return acm;
}

int Graph_kruskal_bound(Graph_t* g)
{
	int bound = 1;
	assert(g != NULL);
	UnionFind_t* uf = UnionFind_new(g->nbNodes);
	bound += UnionFind_new_bound(g->nbNodes);
	Graph_t* acm = Graph_new(g->nbNodes);
	bound += Graph_new_bound(g->nbNodes);
	Edge_t* edges = malloc(g->nbEdges * sizeof(Edge_t));
	int iEdge = 0;
	bound += 3;
	int iNode;
	for(iNode = 0; iNode < g->nbNodes; ++iNode)
	{
		bound += 4;
		Edge_t* e = g->adj[iNode];
		while(e != NULL)
		{
			bound += 5;
			if(e->a < e->b && e->weight >= 0)
				edges[iEdge++] = *e;
			e = e->next;
		}
	}
	bound += quicksort_bound(edges, iEdge);
	quicksort(edges, iEdge);
	bound += 1;
	for(iEdge = 0; iEdge < g->nbEdges; ++iEdge)
	{
		bound += 3 + UnionFind_union_bound(uf, edges[iEdge].a, edges[iEdge].b);
		if(UnionFind_union(uf, edges[iEdge].a, edges[iEdge].b) == 1)
		{
			Graph_add_edge(acm, edges[iEdge].a, edges[iEdge].b, edges[iEdge].weight);
			bound += Graph_add_edge_bound(acm, edges[iEdge].a, edges[iEdge].b, edges[iEdge].weight);
		}
		else
		{
			Graph_add_edge(acm, edges[iEdge].a, edges[iEdge].b, -1);
			bound += Graph_add_edge_bound(acm, edges[iEdge].a, edges[iEdge].b, -1);
		}
	}
	UnionFind_delete(uf);
	bound += UnionFind_delete_bound(uf);
	free(edges);
	return bound + 1;
}

double dst (Graph_t* g, int a, int b)
{
	double x = g->x[a] - g->x[b];
	double y = g->y[a] - g->y[b];
	return sqrt(x*x+y*y);
}

int dst_bound (Graph_t* g, int a, int b)
{
	(void) g; (void) a; (void) b;
	return 10;
}

void Graph_tsm_aux (Graph_t* acm, Graph_t* tsm, char* flag, int* last, int iNode)
{
	Edge_t* e = acm->adj[iNode];
	while(e != NULL)
	{
		if(flag[e->b] == 0 && e->weight >= 0)
		{
			flag[e->b]=1;
			Graph_add_edge(tsm, *last, e->b, dst(tsm, *last, e->b));
			*last = e->b;
			Graph_tsm_aux(acm, tsm, flag, last, e->b);
		}
		e = e->next;
	}
}

Graph_t* Graph_tsm (Graph_t* g)
{
	Graph_t* acm = Graph_kruskal(g);
	Graph_t* tsm = Graph_new(g->nbNodes);
	int iNode;
	for (iNode = 0; iNode < g->nbNodes; ++iNode)
	{
		tsm->x[iNode] = g->x[iNode];
		tsm->y[iNode] = g->y[iNode];
	}
	char* flag = malloc(g->nbNodes * sizeof(char));
	memset(flag, 0, g->nbNodes * sizeof(char));
	int last = 0;
	flag[last]=1;
	Graph_tsm_aux(acm, tsm, flag, &last, 0);
	Graph_add_edge (tsm, last, 0, dst(tsm, last, 0));
	Graph_delete(acm);
	free(flag);
	return tsm;
}

/* Cette fonction n'est que utilitaire, comme le vrai 'aux'
   Elle modifie ses entrées */
int Graph_tsm_aux_bound (Graph_t* acm, Graph_t* tsm, char* flag, int* last, int iNode)
{
	int bound = 1;
	Edge_t* e = acm->adj[iNode];
	bound += 1;
	while(e != NULL)
	{
		bound += 1;
		if(flag[e->b] == 0 && e->weight >= 0)
		{
			bound += 3;
			flag[e->b]=1;
			bound += 2;
			Graph_add_edge(tsm, *last, e->b, dst(tsm, *last, e->b));
			bound += Graph_add_edge_bound(tsm, *last, e->b, dst(tsm, *last, e->b));
			bound += dst_bound(tsm, *last, e->b);
			*last = e->b;
			bound += 1;
			bound += Graph_tsm_aux_bound(acm, tsm, flag, last, e->b);
		}
		e = e->next;
		bound += 4;
	}
	bound += 1;
	return bound;
}

int Graph_tsm_bound (Graph_t* g)
{
	int bound = 1;
	Graph_t* acm = Graph_kruskal(g);
	bound += Graph_kruskal_bound(g);
	Graph_t* tsm = Graph_new(g->nbNodes);
	bound += Graph_new_bound(g->nbNodes);
	int iNode;
	for (iNode = 0; iNode < g->nbNodes; ++iNode)
	{
		tsm->x[iNode] = g->x[iNode];
		tsm->y[iNode] = g->y[iNode];
		bound += 4;
	}
	bound += 2;
	char* flag = malloc(g->nbNodes * sizeof(char));
	bound += 1;
	memset(flag, 0, g->nbNodes * sizeof(char));
	bound += g->nbNodes;
	int last = 0;
	flag[last]=1;
	bound += 2;
	bound += Graph_tsm_aux_bound(acm, tsm, flag, &last, 0);
	Graph_add_edge (tsm, last, 0, dst(tsm, last, 0));
	bound += Graph_add_edge_bound (tsm, last, 0, dst(tsm, last, 0));
	bound += dst_bound (tsm, last, 0);
	Graph_delete(acm);
	bound += Graph_delete_bound(acm);
	free(flag);
	bound += 1;
	return bound;
}


