#pragma once

#include <stdio.h>

#include "Edge.h"

struct Graph_t
{
	double *x, *y;
	Edge_t** adj;
	int nbNodes, nbEdges;
};
typedef struct Graph_t Graph_t;

Graph_t* Graph_new              (int);
int      Graph_new_bound        (int);

void     Graph_delete           (Graph_t*);
int      Graph_delete_bound     (Graph_t*);

void     Graph_add_edge         (Graph_t*, int, int, double);
int      Graph_add_edge_bound   (Graph_t*, int, int, double);

Graph_t* Graph_from_input       (FILE*);
int      Graph_from_input_bound (FILE*);

void     Graph_to_dot           (Graph_t*, FILE*);
int      Graph_to_dot_bound     (Graph_t*, FILE*);

Graph_t* Graph_kruskal          (Graph_t*);
int      Graph_kruskal_bound    (Graph_t*);

Graph_t* Graph_dst              (Graph_t*);
int      Graph_dst_bound        (Graph_t*);

Graph_t* Graph_tsm              (Graph_t* g);
int      Graph_tsm_bound        (Graph_t* g);


