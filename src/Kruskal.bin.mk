${BUILD_DIR}/Kruskal.bin : $(patsubst %,${BUILD_DIR}/%, main.o Graph.o Edge.o UnionFind.o)
${BUILD_DIR}/KruskalBound.bin : $(patsubst %,${BUILD_DIR}/%, main_bound.o Graph.o Edge.o UnionFind.o)
${BUILD_DIR}/TestPlan.bin : $(patsubst %,${BUILD_DIR}/%, testPlan.o Graph.o Edge.o UnionFind.o)
${BUILD_DIR}/TestPlanBound.bin : $(patsubst %,${BUILD_DIR}/%, testPlan_bound.o Graph.o Edge.o UnionFind.o)
