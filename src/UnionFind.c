#include "UnionFind.h"

#include <assert.h>
#include <stdlib.h>

#include <stdio.h>

UnionFind_t* UnionFind_new(int count)
{
	int* repr = malloc(count * sizeof(int));
	int* size = malloc(count * sizeof(int));
	int i;
	for(i = 0; i < count; ++i)
	{
		repr[i] = i;
		size[i] = 1;
	}
	UnionFind_t* uf = malloc(sizeof(UnionFind_t));
	uf->repr = repr;
	uf->size = size;
	uf->count = count;
	return uf;
}

int UnionFind_new_bound(int count)
{
	return 5 + 4 * count + 5;
}

void UnionFind_delete(UnionFind_t* uf)
{
	free(uf->repr);
	free(uf->size);
	free(uf);
}

int UnionFind_delete_bound(UnionFind_t* uf)
{
	return 3;
}

int UnionFind_find(UnionFind_t* uf, int target)
{
	assert(0 <= target && target < uf->count);
	if(uf->repr[target] != target)
		uf->repr[target] = UnionFind_find(uf, uf->repr[target]);
	return uf->repr[target];
}

int UnionFind_find_bound(UnionFind_t* uf, int target)
{
	int bound = 5;
	if(uf->repr[target] != target)
		bound += UnionFind_find_bound(uf, uf->repr[target]);
	return bound;
}

char UnionFind_union(UnionFind_t* uf, int a, int b)
{
	int A = UnionFind_find(uf, a);
	int B = UnionFind_find(uf, b);
	if(A == B)
		return 0;
	if(uf->size[A] < uf->size[B])
		A ^= B, B ^= A, A ^= B;
	uf->repr[B] = A;
	uf->size[A] += uf->size[B];
	return 1;
}

int UnionFind_union_bound(UnionFind_t* uf, int a, int b)
{
	return 2 + UnionFind_find_bound(uf, a) + UnionFind_find_bound(uf, b) + 9;
}
