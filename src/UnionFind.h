#pragma once

struct UnionFind_t
{
	int* repr;
	int* size;
	int count;
};
typedef struct UnionFind_t UnionFind_t;

UnionFind_t* UnionFind_new          (int);
int          UnionFind_new_bound    (int);

void         UnionFind_delete       (UnionFind_t*);
int          UnionFind_delete_bound (UnionFind_t*);

int          UnionFind_find         (UnionFind_t*, int);
int          UnionFind_find_bound   (UnionFind_t*, int);

char         UnionFind_union        (UnionFind_t*, int, int);
int          UnionFind_union_bound  (UnionFind_t*, int, int);
