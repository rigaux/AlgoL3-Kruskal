#include <stdio.h>

#include "Graph.h"

int main(int argc, char** argv)
{
	Graph_t* graph = Graph_from_input(stdin);
	Graph_t* acm = Graph_kruskal(graph);

	Graph_to_dot(acm, stdout);

	Graph_delete(graph);
	Graph_delete(acm);
	return 0;
}
