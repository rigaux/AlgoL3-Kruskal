#include <stdio.h>

#include "Graph.h"

int main(int argc, char** argv)
{
	int bound = 0;
	bound += Graph_from_input_bound(stdin);
	Graph_t* graph = Graph_from_input(stdin);
	bound += Graph_kruskal_bound(graph);
	Graph_t* acm = Graph_kruskal(graph);

	bound += Graph_to_dot_bound(acm, stdout);

	bound += Graph_delete_bound(graph);
	bound += Graph_delete_bound(acm);
	Graph_delete(graph);
	Graph_delete(acm);
	printf("%d\n", bound);
	return 0;
}
