#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "Graph.h"

double distance(double* x, double*y, int i, int j)
{
	double X = x[i] - x[j];
	double Y = y[i] - y[j];
	return sqrt(X * X + Y * Y);
}

void toFile (Graph_t* g, const char* name)
{
	FILE* file = fopen(name, "w");
	Graph_to_dot(g, file);
	fclose(file);
}

int main(int argc, char** argv)
{
	int nbNodes;
	scanf("%d", &nbNodes);
	srand(time(NULL));
	int iNode;
	Graph_t* graph = Graph_new(nbNodes);
	for(iNode = 0; iNode < nbNodes; ++iNode)
	{
		graph->x[iNode] = (rand() % 1000) / 50.;
		graph->y[iNode] = (rand() % 1000) / 50.;
	}
	toFile(graph, "planclean.dot");
	int jNode;
	for(iNode = 0; iNode < nbNodes - 1; ++iNode)
		for(jNode = iNode + 1; jNode < nbNodes; ++jNode)
			Graph_add_edge(graph, iNode, jNode, distance(graph->x, graph->y, iNode, jNode));
	Graph_t* acm = Graph_kruskal(graph);
	for(iNode = 0; iNode < nbNodes; ++iNode)
	{
		acm->x[iNode] = graph->x[iNode];
		acm->y[iNode] = graph->y[iNode];
	}
	toFile(acm, "planacm.dot");
	Graph_t* tsm = Graph_tsm(graph);
	toFile(tsm, "plantsm.dot");
	Graph_delete(graph);
	Graph_delete(acm);
	Graph_delete(tsm);
	return 0;
}
