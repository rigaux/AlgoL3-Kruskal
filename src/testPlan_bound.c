#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "Graph.h"

double distance(double* x, double* y, int i, int j)
{
	double X = x[i] - x[j];
	double Y = y[i] - y[j];
	return sqrt(X * X + Y * Y);
}

int distance_bound(double* x, double* y, int i, int j)
{
	return 8;
}
void toFile (Graph_t* g, const char* name)
{
	FILE* file = fopen(name, "w");
	Graph_to_dot(g, file);
	fclose(file);
}

int toFile_bound (Graph_t* g, const char* c)
{
	(void) c;
	int bound = 1;
	FILE* file = fopen("/dev/null", "w");
	Graph_to_dot(g, file);
	bound += 1 + Graph_to_dot_bound (g, file);
	fclose(file);
	return bound + 1;
}

int main(int argc, char** argv)
{
	int bound = 1;
	int nbNodes;
	scanf("%d", &nbNodes);
	srand(time(NULL));
	int iNode;
	bound += 2;
	Graph_t* graph = Graph_new(nbNodes);
	bound += Graph_new_bound(nbNodes);
	for(iNode = 0; iNode < nbNodes; ++iNode)
	{
		graph->x[iNode] = (rand() % 1000) / 50.;
		graph->y[iNode] = (rand() % 1000) / 50.;
		bound += 10;
	}
	bound += 2;
	toFile(graph, "planclean.dot");
	bound += toFile_bound(graph, "planclean.dot");
	int jNode;
	for(iNode = 0; iNode < nbNodes - 1; ++iNode)
	{
		for(jNode = iNode + 1; jNode < nbNodes; ++jNode)
		{
			Graph_add_edge(graph, iNode, jNode, distance(graph->x, graph->y, iNode, jNode));
			bound += distance_bound (graph->x, graph->y, iNode, jNode) + 2;
			bound += Graph_add_edge_bound(graph, iNode, jNode, distance(graph->x, graph->y, iNode, jNode));
		}
		bound += 2;
	}
	bound += 2;
	Graph_t* acm = Graph_kruskal(graph);
	bound += Graph_kruskal_bound(graph);
	for(iNode = 0; iNode < nbNodes; ++iNode)
	{
		acm->x[iNode] = graph->x[iNode];
		acm->y[iNode] = graph->y[iNode];
		bound += 4;
	}
	bound += 2;
	toFile(acm, "planacm.dot");
	bound += toFile_bound (acm, "planacm.dot");
	Graph_t* tsm = Graph_tsm(graph);
	bound += Graph_tsm_bound(graph);
	toFile(tsm, "plantsm.dot");
	bound += toFile_bound (tsm, "plantsm.dot");
	bound += Graph_delete_bound(graph);
	Graph_delete(graph);
	bound += Graph_delete_bound(acm);
	Graph_delete(acm);
	bound += Graph_delete_bound(tsm);
	Graph_delete(tsm);
	printf("%d\n", bound);
	return 0;
}
